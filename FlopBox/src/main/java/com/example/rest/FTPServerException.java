package com.example.rest;

/**
 * @author Kevin Nguyen
 */

public class FTPServerException extends Exception {

	public FTPServerException(String msg) {
		super(msg);
	}

}
