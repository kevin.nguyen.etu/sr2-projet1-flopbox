package com.example.rest;

/** 
 * @author Kevin Nguyen
 */

import java.io.*;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 * Create an FTP client to interact with the FTP servers.
 */
public class FtpClient extends FTP {

	private FTPClient client;
	private String username;
	private String password;
	private String server;
	private int port;
	private int replyCode;
	private String root = "/tmp/"; // the absolute path to the folder where to download temporarily the file to the flopbox platform.

	/**
	 * Create an FTP client to interact with a FTP server.
	 * 
	 * @param server   the name of the server to connect.
	 * @param port     the port to which to connect to the server.
	 * @param username the username to use with the FTPClient.
	 * @param password the password to use with the FTPClient.
	 */
	public FtpClient(String server, int port, String username, String password) {

		this.client = new FTPClient();
		this.username = username;
		this.password = password;
		this.server = server;
		this.port = port;
	}

	/**
	 * Get the FTPClient.
	 * 
	 * @return The FTPClient.
	 */
	private FTPClient getClient() {
		return this.client;
	}

	/**
	 * Get the username used with the FTPClient.
	 * 
	 * @return the username.
	 */
	private String getUsername() {
		return this.username;
	}

	/**
	 * Get the password used with the FTPClient.
	 * 
	 * @return The password.
	 */
	private String getPassword() {
		return this.password;
	}

	/**
	 * Get the server to which the FTPClient connect with.
	 * 
	 * @return The server.
	 */
	private String getServer() {
		return this.server;
	}

	/**
	 * Get the port to use to connect to the FTP server.
	 * 
	 * @return The port.
	 */
	private int getPort() {
		return this.port;
	}

	/**
	 * Connect to the server.
	 * 
	 * @throws ConnectionServerException if there is a technical error connecting to
	 *                                   the FTP server.
	 * @throws FTPServerException        if the login to the FTP server is refused.
	 */
	public void connect() throws ConnectionServerException, FTPServerException {
		try {
			this.getClient().connect(this.getServer(), this.getPort());
			this.replyCode = this.getClient().getReplyCode(); // on check le code de retour.
			if (!FTPReply.isPositiveCompletion(this.replyCode)) {
				this.disconnectClient();
				throw new ConnectionServerException("Error connection to the FTP server refused.");
			}
		} catch (IOException e) {
			throw new FTPServerException("Error connecting to the FTP server.");
		}
	}

	/**
	 * Login to the server.
	 * 
	 * @throws LoginException     if there is a technical error login to the FTP
	 *                            server.
	 * @throws FTPServerException if the FTP server refuse to login the FTPClient.
	 */
	public void login() throws LoginException, FTPServerException {
		try {
			boolean successLogged = this.getClient().login(this.getUsername(), this.getPassword());
			if (!successLogged) {
				this.disconnectClient();
				throw new LoginException("Error login or password wrong.");
			}
		} catch (IOException e) {
			throw new FTPServerException("Error trying login.");
		}
	}

	/**
	 * Logout from the FTP server the FTPClient
	 * 
	 * @throws LogoutException      if there is a technical error logout to the FTP
	 *                              server.
	 * @throws FTPServerExceptionif the FTP server refuse to logout the FTPClient.
	 */
	public void logout() throws LogoutException, FTPServerException {
		try {
			boolean successLogged = this.getClient().logout();
			if (!successLogged) {
				throw new LogoutException("Error loggin out.");
			}
		} catch (IOException e) {
			throw new FTPServerException("Error trying logout.");
		}
	}

	/**
	 * Disconnect the FTPClient from the FTP server.
	 * 
	 * @throws FTPServerException if there is a technical error making disconnection
	 *                            impossible.
	 */
	public void disconnectClient() throws FTPServerException {
		try {
			this.getClient().disconnect();
		} catch (IOException e) {
			throw new FTPServerException("Error disconnection impossible");
		}
	}

	/**
	 * List all the content of a folder from the FTP server.
	 * 
	 * @param pathFolder The folder from which to display contents.
	 * @return a string containing all the informations of the contents of the
	 *         folder.
	 * @throws FTPServerException if there is a technical error during the listing
	 *                            of the contents.
	 */
	public String listDirectory(String pathFolder) throws FTPServerException {
		try {
			FTPFile[] infoFiles;
			this.getClient().enterLocalPassiveMode();
			infoFiles = this.getClient().listFiles(pathFolder);
			String textInfo = "";
			for (FTPFile info : infoFiles) {
				textInfo += info.getRawListing() + "\n";
			}
			return textInfo;
		} catch (IOException e) {
			throw new FTPServerException("Error listing the directory");
		}
	}

	/**
	 * Download a file from the FTPServer.
	 * 
	 * @param pathFile The path to the file to download from the FTP server.
	 * @throws FTPServerException if there is a technical error downloading the file
	 *                            from the FTP server.
	 */
	public void downloadFile(String pathFile) throws FTPServerException {
		try {
			File file = new File(root + pathFile);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			OutputStream downloaded = new FileOutputStream(file, false);
			this.getClient().setFileType(FTPClient.BINARY_FILE_TYPE);
			this.getClient().enterLocalPassiveMode();
			this.getClient().setAutodetectUTF8(true);
			boolean success = this.getClient().retrieveFile(pathFile, downloaded);
			if (!success) {
				file.delete();
			}
			downloaded.close();
		} catch (IOException e) {
			throw new FTPServerException("Error listing the directory");
		}
	}

	/**
	 * Upload a file to the FTP server.
	 * 
	 * @param pathFile          The path to the file to upload to the FTP server.
	 * @param uploadInputStream the inputstream of the file to upload.
	 * @return True if the upload succeed, false otherwise.
	 * @throws FTPServerException if there is a technical error uploading the file
	 *                            to the server.
	 */
	public boolean uploadFile(String pathFile, InputStream uploadInputStream) throws FTPServerException {
		try {
			this.getClient().setFileType(FTPClient.BINARY_FILE_TYPE);
			this.getClient().enterLocalPassiveMode();
			this.getClient().setAutodetectUTF8(true);
			boolean success = this.getClient().storeFile(pathFile, uploadInputStream);
			uploadInputStream.close();
			return success;
		} catch (IOException e) {
			throw new FTPServerException("Error uploading the file");
		}
	}


	/**
	 * Rename a file from the server.
	 * 
	 * @param oldFilename The current name of the file in the FTP server.
	 * @param newFilename The new name to give to the file in the FTP server.
	 * @return True if the rename is successful, false otherwise.
	 * @throws FTPServerException if there is a technical error renaming the file.
	 */
	public boolean renameFile(String oldFilename, String newFilename) throws FTPServerException {
		try {
			return this.getClient().rename(oldFilename, newFilename);
		} catch (IOException e) {
			throw new FTPServerException("Error renaming the file");
		}
	}

	/**
	 * Rename a directory from the server.
	 * 
	 * @param oldDirectoryName The current name of the directory in the FTP server.
	 * @param newDirectoryName The new name to give to the directory in the FTP server.
	 * @return True if the rename is successful, false otherwise.
	 * @throws FTPServerException if there is a technical error renaming the
	 *                            directory.
	 */
	public boolean renameDirectory(String oldDirectoryName, String newDirectoryName) throws FTPServerException {
		try {
			return this.getClient().rename(oldDirectoryName, newDirectoryName);
		} catch (IOException e) {
			throw new FTPServerException("Error renaming the file");
		}
	}

	/**
	 * Delete a directory from the FTP server.
	 * 
	 * @param pathFolder The path to the folder to remove from the FTP server.
	 * @return True if the delete is successful, false otherwise.
	 * @throws FTPServerException if there is a technical error deleting the folder.
	 */
	public boolean deleteDirectory(String pathFolder) throws FTPServerException {
		try {
			FTPFile[] files;
			this.getClient().enterLocalPassiveMode();
			files = this.getClient().listFiles(pathFolder);
			for (FTPFile file : files) {
				if (file.isDirectory()) {
					deleteDirectory(pathFolder + "/" + file.getName());
				} else {
					String filePath = pathFolder + "/" + file.getName();
					this.getClient().deleteFile(filePath);
				}
			}
			return this.getClient().removeDirectory(pathFolder);
		} catch (IOException e) {
			throw new FTPServerException("Error deleting folder.");
		}
	}

	/**
	 * Create a directory in the FTP server.
	 * 
	 * @param pathFolder The path to the folder to create in the FTP server.
	 * @return True if the creation is successful, false otherwise.
	 * @throws FTPServerException if there is a technical error deleting the folder.
	 */
	public boolean createDirectory(String pathFolder) throws FTPServerException {
		try {
			return this.getClient().makeDirectory(pathFolder);
		} catch (IOException e) {
			throw new FTPServerException("Error deleting folder.");
		}
	}

}
